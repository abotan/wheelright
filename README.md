The task is formulated in Interview exercise (Temperature anaylsis) v1_1.pdf

The chosen approach is described in task_approach.pdf

The code is available as Python script (task.py) and Windows executable (task_windows.exe)

Two input files: Tyre Temperatures.txt, synthetic_data.txt

[Python] Run task.py from shell 

```bash
python task.py [filename] 
```

[Windows] Run task_windows.exe from command prompt


```
task_windows.exe [filename]
 ```