# import numpy as np
from numpy import shape, loadtxt, isnan, where, take, array, arange, histogram, argmax, atleast_1d, hstack, unique, zeros, argsort, sort, exp
from scipy.optimize import curve_fit
import sys



class TyreTemperature(object):
	"""
	Analyse temperature profile in car tyres.
	"""
	
	def __init__(self,parameters):

		for k,v in parameters.iteritems():
		    setattr(self, k, v)

		dataset = self.read_file(self.filename)

		left=0; right=1

		for side in [left,right]:
			print '\n\nSide {}'.format('Left' if side == 0 else 'Right')

			self.dataset_side = dataset[:,side]

			self.cleaning()

			self.compute_ambient_temperature()
			print 'Ambient temperature: {:,.1f}'.format(self.ambient_temperature)

			self.scale_temperatures()

			self.detect_peaks()

			for tyre in range(shape(self.peaks_ind)[0]):

				p = self.fit_to_gauss(self.peaks_ind[tyre])

				if p[2] > self.S0: # Check 
					print '\n....... Tyre {}'.format(tyre+1)

					max_temp = (self.ambient_temperature + 1) + self.dataset_side[self.peaks_ind[tyre]]
					aver_temp = self.compute_average_temperature(p) + (self.ambient_temperature + 1)
					
					print '....... Max temperature: {:,.1f}'.format(max_temp)
					print '....... Average temperature: {:,.1f}'.format(aver_temp)


	@staticmethod
	def read_file(filename):
		"""
		Assume the file format is constant (made by a sensor manager)
		"""
		return loadtxt(filename,skiprows=1)


	def cleaning(self):
		"""
		NaN values replaced by -100
		Isolated peaks removed
		"""

		#Find indicies that you need to replace
		inds = where(isnan(self.dataset_side))

		#Place -100 in the indices
		self.dataset_side[inds] = take(-100,inds)


		dy=[]
		dy.append(0)

		for i in xrange(1,shape(self.dataset_side)[0] - 1):
			dy.append(min(abs(self.dataset_side[i] - self.dataset_side[i-1]), abs(self.dataset_side[i+1] - self.dataset_side[i])))


		dy=array(dy)
		indexes=array(where(dy > self.jump_threshold))
		
		for i in indexes[0]:

		    avg = 0.5*(self.dataset_side[i-1] + self.dataset_side[i+1])
		    self.dataset_side[i] = int(avg)



	def compute_ambient_temperature(self):
		"""
		Compute ambient temperature
		"""

		n, bin_edges = histogram(self.dataset_side, bins=arange(self.dataset_side.min(), self.dataset_side.max() + 1.0, 1.0))

		self.ambient_temperature = bin_edges[argmax(n)]

	def scale_temperatures(self):

		scale = self.ambient_temperature + 1

		ind = where(self.dataset_side<scale)
		self.dataset_side[ind] = scale
		self.dataset_side[:] = self.dataset_side[:] - scale


	def detect_peaks(self):

		"""
		Detect peaks in data based on their amplitude

		"""

		x = self.dataset_side
		mpd = self.mpd
		mph = self.mph

		x = atleast_1d(x).astype('float64')
		if x.size < 3:
		    return array([], dtype=int)

		# find indexes of all peaks
		dx = x[1:] - x[:-1]

		ine, ire, ife = array([[], [], []], dtype=int)

		ire = where((hstack((dx, 0)) <= 0) & (hstack((0, dx)) > 0))[0]
		# ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
		ind = unique(hstack((ine, ire, ife)))

		# first and last values of x cannot be peaks
		if ind.size and ind[0] == 0:
		    ind = ind[1:]
		if ind.size and ind[-1] == x.size-1:
		    ind = ind[:-1]
		# remove peaks < minimum peak height
		if ind.size and mph is not None:
		    ind = ind[x[ind] >= mph]

		# detect small peaks closer than minimum peak distance
		if ind.size and mpd > 1:
		    ind = ind[argsort(x[ind])][::-1]  # sort ind by peak height
		    idel = zeros(ind.size, dtype=bool)
		    for i in range(ind.size):
		        if not idel[i]:
		            idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
		                & (True)
		            idel[i] = 0  # Keep current peak
		    # remove the small peaks and sort back the indexes by their occurrence
		    ind = sort(ind[~idel])
		
		self.peaks_ind = ind


	@staticmethod
	def gauss_func(x,a,x0,sigma):

		return a*exp(-(x-x0)**2/(2*sigma**2))


	def fit_to_gauss(self, xc):

		mpd = self.mpd
		y = self.dataset_side

		x0=max(xc-mpd,0)
		x1=min(xc+mpd,len(y)-1)        
		y_cutted=array(y[x0:x1])
		n=len(y_cutted)
		x_cutted=range(n)

		popt,pcov = curve_fit(self.gauss_func,x_cutted,y_cutted,p0=[y[xc],x_cutted[mpd],10])

		return popt

		
	def compute_average_temperature(self,p):
		
		"""
		Compute average temperature per tyre
		"""
		

		interval1 = 2*p[2]
		area = p[0]*p[2]/0.3989

		average_temp = (area / interval1) * 0.68


		return average_temp

		
if __name__ == "__main__":

	if len(sys.argv) < 2:
		raise ValueError("You must provide a filename of the dataset to be analysed.")

	filename = sys.argv[1]

	params = {  'filename' : filename,
                'jump_threshold': 5.0,
                "mph": 4.0,
                "mpd": 100,
                "S0": 10
                }

	TyreTemperature(params)

